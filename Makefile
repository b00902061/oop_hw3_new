main: ./src/*.java
	javac -d ./ ./src/*.java

run: ./*.class
	java P00BBS

clean:
	rm -r *.class
