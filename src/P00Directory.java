import java.io.*;
import java.util.*;
//
public class P00Directory extends P00Board{
    private static Vector<P00Board> board_table = new Vector<P00Board>();
    private Vector<P00Board> boards;
    private P00Board split;


    public P00Directory(String init_name){
        super(init_name);
        boards = new Vector<P00Board>();
        split = new P00Board("==========================================");
        board_table.add(this);
        return;
    }

    public void add(P00Article new_article){
        return;
    }

    public void add(P00Board new_board){
        boards.add(new_board);
        board_table.add(new_board);
        return;
    }

    public void add(P00Directory new_directory){
        boards.add(new_directory);
        board_table.add(new_directory);
        return;
    }

    public void add_split(){
        boards.add(split);
        return;
    }

    public void del(int pos){
        if((pos >= this.length()) || (pos < 0))
            return;
        boards.removeElementAt(pos);
        return;
    }

    public void move(int src,int dest){
        if((src >= this.length()) || (dest >= this.length()) || (src < 0) || (dest < 0))
            return;
        if(src == dest)
            return;
        if(src < dest){
            boards.insertElementAt(boards.get(src),dest+1);
            boards.removeElementAt(src);
        }else{
            boards.insertElementAt(boards.get(src),dest);
            boards.removeElementAt(src+1);
        }
        return;
    }

    public int length(){
        return boards.size();
    }

    public void show(){
        for(int i = 0;i < show_limit;i++,System.out.print("\n")){
            System.out.print("("+(show_n + i)+") ");
            if(show_n+i < boards.size())
                boards.get(show_n+i).print_title();
            else System.out.print("NULL");
        }
        return;
    }

    public int find(String title){
        for(int i = 0;i < boards.size();i++){
            if(boards.get(i).equals(title)){
                System.out.println("You have had the board/directory ["+title+"] yet.");
                return 1;
            }
        }
        for(int i = 0;i < board_table.size();i++){
            if(board_table.get(i).equals(title)){
                boards.add(board_table.get(i));
                return 1;
            }
        }
        return -1;
    }

    public void demo(String usr_name){
        System.out.print("########################################\nwelcome to Directory : ");
        this.print_title();
        System.out.print("\n########################################\n");
        this.show();

        Scanner input = new Scanner(System.in);
        int ID;
        int to;
        String command;
        String title;

        System.out.println("\n########################################\ncommand:(AB)add_board (AD)add_directory (AS)add_spliting_line (D)del (G)goto (M)move (PU)page_up (PD)page_down (Q)quit");
        while(!((command = new String(input.nextLine())).equals("Q"))){
            if(command.equals("AD") || command.equals("ad")){
                System.out.print("Create a new Directory.\nName : ");
                title = new String(input.nextLine());
                if(this.find(title) == -1){
                    P00Directory temp = new P00Directory(title);
                    this.add(temp);
                }
            }
            else if(command.equals("AB") || command.equals("ab")){
                System.out.print("Create a new Board.\nName : ");
                title = new String(input.nextLine());
                if(this.find(title) == -1){
                    P00Board temp = new P00Board(title);
                    this.add(temp);
                }
            }
            else if(command.equals("AS") || command.equals("as")){
                System.out.println("Create a spliting line.");
                this.add_split();
            }
            else if(command.equals("D") || command.equals("d")){
                System.out.print("del : ");
                ID  = input.nextInt();
                System.out.print("You want to delete ("+ID+")");
                this.boards.get(ID).print_title();
                System.out.print(" ?[Y/n]");
                title = new String(input.nextLine()); //get rid of '\n'
                title = new String(input.nextLine());
                if(title.equals("Y") || title.equals("y") || title.equals(""))
                    this.del(ID);
            }
            else if(command.equals("G") || command.equals("g")){
                System.out.print("Go to : ");
                ID  = input.nextInt();
                title = new String(input.nextLine()); //get rid of '\n'
                if(!(ID >= this.length() || ID < 0)){
                    System.out.print("\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n");
                    this.boards.get(ID).demo(usr_name);
                }
            }
            else if(command.equals("M") || command.equals("m")){
                System.out.print("move from : ");
                ID  = input.nextInt();
                System.out.print("to : ");
                to = input.nextInt();
                this.move(ID,to);
                title = new String(input.nextLine()); //get rid of '\n'
            }else if(command.equals("PD") || command.equals("pd")){
                if(show_n < boards.size() - show_limit)
                    show_n += show_limit;
            }else if(command.equals("PU") || command.equals("pu")){
                if(show_n > 0)
                    show_n -= show_limit;
            }
            /*if(command.equals("N")){
                System.out.println("there is/are "+this.length()+" member(s) in the Directory.");
            }*/
            System.out.print("\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n########################################\nwelcome to Directory : ");
            this.print_title();
            System.out.print("\n########################################\n");
            this.show();
            System.out.println("########################################\ncommand:(AB)add_board (AD)add_directory (AS)add_spliting_line (D)del (G)goto (M)move (PU)page_up (PD)page_down (Q)quit");
        }
        return;
    }

    public static void main(String[] argv){
        P00Directory KPP = new P00Directory("KPP");
        KPP.demo("KPP");
        return;
    }
}
