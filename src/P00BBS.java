import java.io.*;
import java.util.*;
//
public class P00BBS{
    public static void main(String[] argv){
        String name;
        Vector<P00Directory> usr_home = new Vector<P00Directory>();
        Vector<String> usr_name = new Vector<String>();
        int usr_ID;
        Scanner input = new Scanner(System.in);
        P00Directory main_page;
        P00Directory sample_directory = new P00Directory("sample_directory");
        P00Board sample_board = new P00Board("sample_board");
        P00Article sample_article = new P00Article(-1,"sample_article","kpp","\nThanks for use sample article.:)");
        sample_board.add(sample_article);

        System.out.println("\n------------------------------------------------\nPlease enter your name or you can set (Q) to leave");
        while(!((name = new String(input.nextLine())).equals("Q"))){
            System.out.println("------------------------------------------------\nHello, "+name+". Welcome to P00BBS!\n");
            for(usr_ID = 0;usr_ID < usr_name.size();usr_ID++){
                if(usr_name.get(usr_ID).equals(name))
                    break;
            }
            if(usr_ID == usr_name.size()){
                main_page = new P00Directory(name+"'s home");
                main_page.add(sample_directory);
                main_page.add(sample_board);
                usr_home.add(main_page);
                usr_name.add(name);
            }
            usr_home.get(usr_ID).demo(name);
            System.out.println("\n------------------------------------------------\nPlease enter your name or you can set (Q) to leave");
        }
        return;
    }
}

