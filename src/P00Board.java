import java.io.*;
import java.util.*;
//
public class P00Board{
    private String name;
    private int ID;
    private Vector<P00Article> articles;
    public int show_n;
    public int show_limit;

    public P00Board(String init_name){
        name = init_name;
        ID = 0;
        show_n = 0;
        show_limit = 16;
        articles = new Vector<P00Article>();
        return;
    }

    public void add(P00Article new_article){
        articles.add(new_article);
        return;
    }

    public void del(int pos){
        if((pos >= this.length()) || (pos < 0))
            return;
        articles.removeElementAt(pos);
        return;
    }

    public void move(int src,int dest){
        if((src >= this.length()) || (dest >= this.length()) || (src < 0) || (dest < 0))
            return;
        if(src == dest)
            return;
        if(src < dest){
            articles.insertElementAt(articles.get(src),dest+1);
            articles.removeElementAt(src);
        }else{
            articles.insertElementAt(articles.get(src),dest);
            articles.removeElementAt(src+1);
        }
        return;
    }

    public int length(){
        return articles.size();
    }

    public boolean equals(String compare_name){
        return this.name.equals(compare_name);
    }

    public void show(){
        for(int i = 0;i < show_limit;i++,System.out.print("\n")){
            System.out.print("("+(show_n + i)+")");
            if(show_n + i < articles.size())
                articles.get(show_n+i).list();
            else System.out.print(" NULL");
        }
        return;
    }

    public void print_title(){
        System.out.print(name);
        return;
    }

    public void demo(String usr_name){
        System.out.print("########################################\nwelcome to Board : ");
        this.print_title();
        System.out.print("\n########################################\n");
        this.show();

        Scanner input = new Scanner(System.in);
        int ID;
        int to;
        String command;
        String title;
        String content;
        String temp_text;

        System.out.println("\n########################################\ncommand:(A)add_article (AR)arrow (B)bool (D)del (G)goto (M)move (P)push (PU)page_up (PD)page_down (Q)quit");
        while(!((command = new String(input.nextLine())).equals("Q"))){
            if(command.equals("A") || command.equals("a")){
                System.out.print("Create a new article.\nTitle : ");
                title = new String(input.nextLine());
                System.out.println("Content(Press only \"Q\" to leave) :");
                content = new String("");
                while(!((temp_text = new String(input.nextLine())).equals("Q"))){
                    content = new String(content+"\n"+temp_text);
                }
                P00Article temp = new P00Article(this.ID,title,usr_name,content);
                this.ID++;
                if(this.ID >= 1000)
                    this.ID = 0;
                this.add(temp);
            }
            else if(command.equals("AR") || command.equals("ar")){
                System.out.print("Add an arrow in : ");
                ID  = input.nextInt();
                title = new String(input.nextLine()); //get rid of '\n'
                if(!(ID >= this.length() || ID < 0)){
                    System.out.print("-> ");
                    temp_text = new String(input.nextLine());
                    this.articles.get(ID).arrow(temp_text,usr_name);
                }
            }
            else if(command.equals("B") || command.equals("b")){
                System.out.print("Add an boo in : ");
                ID  = input.nextInt();
                title = new String(input.nextLine()); //get rid of '\n'
                if(!(ID >= this.length() || ID < 0)){
                    System.out.print(":( ");
                    temp_text = new String(input.nextLine());
                    this.articles.get(ID).boo(temp_text,usr_name);
                }
            }
            else if(command.equals("P") || command.equals("p")){
                System.out.print("Add an push in : ");
                ID  = input.nextInt();
                title = new String(input.nextLine()); //get rid of '\n'
                if(!(ID >= this.length() || ID < 0)){
                    System.out.print(":) ");
                    temp_text = new String(input.nextLine());
                    this.articles.get(ID).push(temp_text,usr_name);
                }
            }
            else if(command.equals("D") || command.equals("d")){
                System.out.print("del : ");
                ID  = input.nextInt();
                System.out.print("You want to delete ("+ID+")");
                this.articles.get(ID).print_title();
                System.out.print(" ?[Y/n]");
                title = new String(input.nextLine()); //get rid of '\n'
                title = new String(input.nextLine());
                if(title.equals("Y") || title.equals("y") || title.equals(""))
                    this.del(ID);
            }
            else if(command.equals("G") || command.equals("g")){
                System.out.print("Go to : ");
                ID  = input.nextInt();
                title = new String(input.nextLine()); //get rid of '\n'
                if(!(ID >= this.length() || ID < 0))
                    this.articles.get(ID).demo(usr_name);
            }
            else if(command.equals("M") || command.equals("m")){
                System.out.print("move from : ");
                ID  = input.nextInt();
                System.out.print("to : ");
                to = input.nextInt();
                this.move(ID,to);
                title = new String(input.nextLine()); //get rid of '\n'
            }else if(command.equals("PD") || command.equals("pd")){
                if(show_n < articles.size() - show_limit)
                    show_n += show_limit;
            }else if(command.equals("PU") || command.equals("pu")){
                if(show_n > 0)
                    show_n -= show_limit;
            }
            System.out.print("\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n########################################\nwelcome to Board : ");
            this.print_title();
            System.out.print("\n########################################\n");
            this.show();
            System.out.println("########################################\ncommand:(A)add_article (AR)arrow (B)bool (D)del (G)goto (M)move (P)push (PU)page_up (PD)page_down (Q)quit");
        }
        return;
    }

    public static void main(String[] argv){
        P00Board KPP = new P00Board("KPP");
        KPP.demo("KPP");
        return;
    }
}
