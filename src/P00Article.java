import java.io.*;
import java.util.*;
//
public class P00Article{
    private int ID;
    private int MAXEVAL;
    private String title;
    private String author;
    private String content;
    private int evaluation_count;
    private Vector<String> evaluation_messages;

    public P00Article(int init_ID,String init_title,String init_author,String init_content){
        MAXEVAL = 16;
        ID = init_ID;
        title = init_title;
        author = init_author;
        content = init_content;
        evaluation_count = 0;
        evaluation_messages = new Vector<String>();
        return;
    }

    public void push(String text,String name){
        if(this.author.equals(name)){
            this.arrow(text,name);
            return;
        }
        evaluation_count++;
        if(text.length() <= MAXEVAL)
            evaluation_messages.add(":) "+name+" : "+text);
        else evaluation_messages.add(":) "+name+" : "+text.substring(0,MAXEVAL));
        return;
    }

    public void boo(String text,String name){
        if(this.author.equals(name)){
            this.arrow(text,name);
            return;
        }
        evaluation_count--;
        if(text.length() <= MAXEVAL)
            evaluation_messages.add(":( "+name+" : "+text);
        else evaluation_messages.add(":( "+name+" : "+text.substring(0,MAXEVAL));
        return;
    }

    public void arrow(String text,String name){
        if(text.length() <= MAXEVAL)
            evaluation_messages.add("-> "+name+" : "+text);
        else evaluation_messages.add("-> "+name+" : "+text.substring(0,MAXEVAL));
        return;
    }

    public void show(){
        System.out.println("\nID : "+ID+"\nTitle : "+title+"\nAuthor : "+author+"\n------------------------------------\nContent :"+content+"\n------------------------------------\nEvaluation Messages :");
        for(int i = 0;i < evaluation_messages.size();i++)
            System.out.println(evaluation_messages.get(i));
        return;
    }

    public void list(){
        System.out.print(" "+evaluation_count+" | "+author+" | "+title);
        return;
    }

    public void print_title(){
        System.out.print(title);
        return;
    }

    public void demo(String name){
        this.show();

        Scanner input = new Scanner(System.in);
        String command;
        String temp_text;

        System.out.println("\n########################################\ncommand:(A)arrow (B)bool (P)push (Q)quit");
        while(!((command = new String(input.nextLine())).equals("Q"))){
            if(command.equals("A") || command.equals("a")){
                System.out.print("-> ");
                temp_text = new String(input.nextLine());
                this.arrow(temp_text,name);
                return;
            }
            else if(command.equals("B") || command.equals("b")){
                System.out.print(":( ");
                temp_text = new String(input.nextLine());
                this.boo(temp_text,name);
                return;
            }
            else if(command.equals("P") || command.equals("p")){
                System.out.print(":) ");
                temp_text = new String(input.nextLine());
                this.push(temp_text,name);
                return;
            }
        }
        return;
    }

    public static void main(String[] argv){
        P00Article KPP = new P00Article(10,"KPP","kpp","XDD");
        KPP.demo("KPP");
        return;
    }
}
